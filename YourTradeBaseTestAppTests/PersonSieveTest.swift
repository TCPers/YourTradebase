//
//  PersonSieveTest.swift
//  YourTradeBaseTestAppTests
//
//  Created by Joel Barker on 20/06/2018.
//  Copyright © 2018 Talking Cucumber Ltd. All rights reserved.
//

import XCTest

class PersonSieveTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testQ1() {
        
        let jsonPath = Bundle(for: JSONImporterTest.self).path(forResource: "people", ofType: "json")
        
        Logging.JLog(message: "jsonPath : \(String(describing: jsonPath))")
        
        
        let people = JSONImporter.shared.importJSON(filePath: jsonPath!)
        
        Logging.JLog(message: "products : \(people.count)")

        let sieve = PersonSieve (thePeople: people)
        
        let latLon = [51.450167, -2.594678]
        
        let countryWanted = "England"
        
        let peopleJsonArray = sieve.findForLocation(latLon: latLon, distanceWanted: 100000, countryWanted: countryWanted)
 
        Logging.JLog(message: "peopleJsonArray : \(peopleJsonArray.count)")
        
        XCTAssert(peopleJsonArray.count == 16)
        
        Logging.JLog(message: "avgValue : \(sieve.avgValue)")
        XCTAssert(sieve.avgValue == 2408.50875)
        
        let firstPerson = peopleJsonArray.first
        let lastPerson = peopleJsonArray.last

        let startValue = firstPerson! ["value"] as? Decimal
        let endValue = lastPerson! ["value"] as? Decimal
        
        XCTAssert(endValue! < startValue!)
        
        
    }
    
    func testQ2() {
        
        let jsonPath = Bundle(for: JSONImporterTest.self).path(forResource: "people", ofType: "json")
        
        Logging.JLog(message: "jsonPath : \(String(describing: jsonPath))")
        
        
        let people = JSONImporter.shared.importJSON(filePath: jsonPath!)
        
        Logging.JLog(message: "products : \(people.count)")
        
        let sieve = PersonSieve (thePeople: people)
        
        let latLon = [51.450167, -2.594678]
        
        let countryWanted = ""
        
        let peopleJsonArray = sieve.findForLocation(latLon: latLon, distanceWanted: 200000, countryWanted: countryWanted)
        
        Logging.JLog(message: "peopleJsonArray : \(peopleJsonArray.count)")
        
        XCTAssert(peopleJsonArray.count == 58)
        
        Logging.JLog(message: "avgValue : \(sieve.avgValue.description)")
        
        let firstPerson = peopleJsonArray.first
        let lastPerson = peopleJsonArray.last
        
        let startValue = firstPerson! ["value"] as? Decimal
        let endValue = lastPerson! ["value"] as? Decimal
        
        XCTAssert(endValue! < startValue!)
        
        
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
