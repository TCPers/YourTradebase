//
//  PersonTest.swift
//  YourTradeBaseTestAppTests
//
//  Created by Joel Barker on 19/06/2018.
//  Copyright © 2018 Talking Cucumber Ltd. All rights reserved.
//

import XCTest

class PersonTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testProductFromJSON () {
        
        let jsonDict = [
            "id": "5a00487905c99b6667e5e1ea",
            "value": "3749.61",
            "name": [
                "first": "Riddle",
                "last": "Nixon"
            ],
            "location": [
                "latitude": "51.077801",
                "longitude": "-3.082931"
            ],
            "company": "Isodrive",
            "email": "riddle.nixon@isodrive.me",
            "address": "177 Wyckoff Avenue, Marienthal, Guam, 1832",
            "country": "england",
            ] as [String:Any]
        
        
        let person = Person (theJsonDict: jsonDict)
        
        Logging.JLog(message: "person : \(person.description)")
        
        XCTAssert(person.id == "5a00487905c99b6667e5e1ea")
        XCTAssert(person.value == 3749.61)
        XCTAssert(person.firstName == "Riddle")
        XCTAssert(person.lastName == "Nixon")
        XCTAssert(person.company == "Isodrive")
        XCTAssert(person.email == "riddle.nixon@isodrive.me")
        XCTAssert(person.address == "177 Wyckoff Avenue, Marienthal, Guam, 1832")
        XCTAssert(person.country == "england")
        
        XCTAssert(person.location[0] == 51.077801)
        XCTAssert(person.location[1] == -3.082931)
        
        let output = person.getBasicDetails()
        
        Logging.JLog(message: "output : \(output)")
        
        XCTAssert((output ["id"] as! String) == "5a00487905c99b6667e5e1ea")
        XCTAssert((output ["full name"] as! String) == "Riddle Nixon")
        XCTAssert((output ["value"] as! Decimal) == 3749.61)
        XCTAssert((output ["email"] as! String) == "riddle.nixon@isodrive.me")
        
        let london = (51.5074, 0.1278)
        
        let distance = person.distanceFrom(lat: london.0, lon: london.1)
        
        Logging.JLog(message: "distance : \(distance)")
        
        XCTAssert(distance == Double(228163.8043089821))
        
        
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
