//
//  DistanceUtilsTest.swift
//  YourTradeBaseTestAppTests
//
//  Created by Joel Barker on 19/06/2018.
//  Copyright © 2018 Talking Cucumber Ltd. All rights reserved.
//

import XCTest

class DistanceUtilsTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testAmsterdamToNYC() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let amsterdam = (52.3702, 4.8952)
        let newYork = (40.7128, -74.0059)
        
        let distance = DistanceUtils.shared.haversineDinstance  (la1: amsterdam.0, lo1: amsterdam.1, la2: newYork.0, lo2: newYork.1)
        
        // should be around 5,857 km
        XCTAssert(distance == Double (5859270.9055619))
        
        Logging.JLog(message: "distance : \(distance)")
    }
    
    func testLondonToBrighton() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let london = (51.5074, 0.1278)
        let brighton = (50.8225, 0.1372)
        
        let distance = DistanceUtils.shared.haversineDinstance  (la1: london.0, lo1: london.1, la2: brighton.0, lo2: brighton.1)
        
        // should be around 76 km
        XCTAssert(distance == Double (76117.7247661622))
        
        Logging.JLog(message: "distance : \(distance)")
    }
    

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
