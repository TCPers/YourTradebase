//
//  JSONImporterTest.swift
//  YourTradeBaseTestAppTests
//
//  Created by Joel Barker on 19/06/2018.
//  Copyright © 2018 Talking Cucumber Ltd. All rights reserved.
//

import XCTest

class JSONImporterTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testImportJson () {
        
        let jsonPath = Bundle(for: JSONImporterTest.self).path(forResource: "people", ofType: "json")
        
        Logging.JLog(message: "jsonPath : \(String(describing: jsonPath))")
        
        
        let people = JSONImporter.shared.importJSON(filePath: jsonPath!)
        
        Logging.JLog(message: "products : \(people.count)")
        
        var sawPerson = false
        
        for person in people {
            
            if !sawPerson {
                sawPerson = (person.id == "5a00487905c99b6667e5e1ea") && (person.email == "riddle.nixon@isodrive.me")
            }
            
            
        }
        
        XCTAssert(sawPerson)
        
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
