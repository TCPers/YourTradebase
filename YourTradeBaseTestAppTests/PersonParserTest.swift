//
//  PersonParserTest.swift
//  YourTradeBaseTestAppTests
//
//  Created by Joel Barker on 19/06/2018.
//  Copyright © 2018 Talking Cucumber Ltd. All rights reserved.
//

import XCTest

class PersonParserTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testParseJSON() {
        
        
        let jsonDict = [
            "id": "5a00487905c99b6667e5e1ea",
            "value": "3749.61",
            "name": [
                "first": "Riddle",
                "last": "Nixon"
            ],
            "location": [
                "latitude": "51.077801",
                "longitude": "-3.082931"
            ],
            "company": "Isodrive",
            "email": "riddle.nixon@isodrive.me",
            "address": "177 Wyckoff Avenue, Marienthal, Guam, 1832",
            "country": "england",
            ] as [String:Any]
        
        
        let parser = PersonParser.init(theJsonDict: jsonDict)
        
        XCTAssert(parser.getId() == "5a00487905c99b6667e5e1ea")
        XCTAssert(parser.getValue() == 3749.61)
        XCTAssert(parser.getFirstName() == "Riddle")
        XCTAssert(parser.getLastName() == "Nixon")
        XCTAssert(parser.getCompany() == "Isodrive")
        XCTAssert(parser.getEmail() == "riddle.nixon@isodrive.me")
        XCTAssert(parser.getAddress() == "177 Wyckoff Avenue, Marienthal, Guam, 1832")
        XCTAssert(parser.getCountry() == "england")
        
        XCTAssert(parser.getLocation()[0] == 51.077801)
        XCTAssert(parser.getLocation()[1] == -3.082931)
        
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
