#  Using

Load a JSON file using the "File" -> "Open" menu item. Then fill in the desired values. Press the "Convert" button to extract data. A Save dialog will open to save the JSON output. The Average Value can be seen on screen.

# Assumptions

1. I have used Decimal rather than Double as money is being dealt with. 
2. "Within" is defined as a distance less than the total distance. Eg, less than the stated distance.
