//
//  ViewController.swift
//  YourTradeBaseTestApp
//
//  Created by Joel Barker on 19/06/2018.
//  Copyright © 2018 Talking Cucumber Ltd. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    @IBOutlet weak var peopleLoadedLabel: NSTextField!
    
    @IBOutlet weak var avgValueLabel: NSTextField!
    
    @IBOutlet weak var countryTextField: NSTextField!
    
    @IBOutlet weak var distanceTextField: NSTextField!
    
    @IBOutlet weak var latTextField: NSTextField!
    
    @IBOutlet weak var lonTextField: NSTextField!
    
    var peopleGot = [Person] ()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let nc = NotificationCenter.default
        nc.addObserver(
            self,
            selector: #selector(gotPeopleData(noti:)),
            name: Notification.Name(rawValue:"com.talkingcucumbertltd.gotfile"), object: nil
        )
        
        
        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


    @objc func gotPeopleData (noti: Notification) {
        
        self.peopleGot = noti.userInfo! ["people"] as! [Person]
        
        peopleLoadedLabel.stringValue = self.peopleGot.count.description
        
    }
    
    @IBAction func calculateButtonPressed(_ sender: Any) {
        
        if self.peopleGot.count == 0 {
            _ = self.dialogOKCancel(question: "Hello", text: "Please load a JSON File")
            return
        }
        
        
        let latStr = self.latTextField.stringValue
        let lonStr = self.lonTextField.stringValue
        let distanceWanted = self.distanceTextField.stringValue
        let countryWanted = self.countryTextField.stringValue
        
        if Double (distanceWanted) == nil {
            _ = self.dialogOKCancel(question: "Hello", text: "Please enter a valid Double for Distance.")
            return
        }

        
        
        let lat = Double (latStr)
        let lon = Double (lonStr)
        let distance = Double (distanceWanted)! * Double (1000)

        if lat == nil {
            _ = self.dialogOKCancel(question: "Hello", text: "Please enter a valid Double for Lattitude.")
            return
        }

        if lon == nil {
            _ = self.dialogOKCancel(question: "Hello", text: "Please enter a valid Double for Longitude.")
            return
        }
        
        
        
        
        let latLon = [lat, lon]
        
        Logging.JLog(message: "lat : \(String(describing: lat)), lon : \(String(describing: lon)), distance : \(String(describing: distance)), countryWanted : \(countryWanted)")
        
        
        let sieve = PersonSieve (thePeople: self.peopleGot)
        
        let peopleJsonArray = sieve.findForLocation(latLon: latLon as! [Double], distanceWanted: distance, countryWanted: countryWanted)
        
        Logging.JLog(message: "peopleFound : \(peopleJsonArray.count)")
        
        self.avgValueLabel.stringValue = sieve.avgValue.description
        
        var jsonStr = ""
        
        do {
        
            let data = try JSONSerialization.data(withJSONObject: peopleJsonArray, options: [])
            jsonStr = String(data: data, encoding: .utf8)!
        
        } catch  {
            print("error trying to convert data to JSON")
            
        }
        
        let saveDialog = NSSavePanel()
        
        
        saveDialog.begin { (result) -> Void in
            
            if result.rawValue == NSApplication.ModalResponse.OK.rawValue {
                let filename = saveDialog.url
                
                do {
                    try jsonStr.write(to: filename!, atomically: true, encoding: String.Encoding.utf8)
                } catch {
                    // failed to write file (bad permissions, bad filename etc.)
                }
                
            }
        }
        
       
        
        
    }
    
    func dialogOKCancel(question: String, text: String) -> Bool {
        
        let alert = NSAlert()
        alert.messageText = question
        alert.informativeText = text
        alert.alertStyle = .warning
        alert.addButton(withTitle: "OK")
        return alert.runModal() == .alertFirstButtonReturn
    }
    
    
}

