//
//  Person.swift
//  YourTradeBaseTestApp
//
//  Created by Joel Barker on 19/06/2018.
//  Copyright © 2018 Talking Cucumber Ltd. All rights reserved.
//

import Cocoa

class Person: NSObject {
    
    var id = ""

    var value = Decimal (0.00)

    var firstName = ""

    var lastName = ""

    var location = [Double] ()

    var company = ""

    var email = ""

    var address = ""

    var country = ""

    override init() {
        
    }
    
    init (theJsonDict: [String : Any]) {
        super.init()
        
        let parser = PersonParser (theJsonDict: theJsonDict)
        
        self.id = parser.getId()
        self.value = parser.getValue()
        self.firstName = parser.getFirstName()
        self.lastName = parser.getLastName()
        self.location = parser.getLocation()
        self.company = parser.getCompany()
        self.email = parser.getEmail()
        self.address = parser.getAddress()
        self.country = parser.getCountry()
        
        
    }
    
    override var description : String {
        
        var descrStr = "id : \(self.id), value : \(self.value), company : \(self.company)"
        
        descrStr = descrStr + ", firstName : \(self.firstName)"
        descrStr = descrStr + ", lastName : \(self.lastName)"
        descrStr = descrStr + ", location : \(self.location)"
        descrStr = descrStr + ", company : \(self.company)"
        descrStr = descrStr + ", address : \(self.address)"
        descrStr = descrStr + ", country : \(self.country)"
        
        return descrStr
    }

    func distanceFrom (lat: Double, lon: Double) -> Double {
        
        let myLatLon = (self.location[0], self.location[1])
        let toDist = (lat, lon)
        
        let distance = DistanceUtils.shared.haversineDinstance  (la1: myLatLon.0, lo1: myLatLon.1, la2: toDist.0, lo2: toDist.1)
        
        return distance
    }
    
    func getBasicDetails () -> [String:Any] {
        
        let output = [
            "id" : self.id,
            "full name" : self.firstName + " " + self.lastName,
            "value" : self.value,
            "email" : self.email
        ] as [String:Any]
        
        return output
        
    }
    
}
