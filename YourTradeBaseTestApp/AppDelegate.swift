//
//  AppDelegate.swift
//  YourTradeBaseTestApp
//
//  Created by Joel Barker on 19/06/2018.
//  Copyright © 2018 Talking Cucumber Ltd. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    @IBAction func openPressed(_ sender: NSMenuItem) {
        
        Logging.JLog(message: "openPressed")
        
        let dialog = NSOpenPanel();
        
        dialog.title                   = "Choose a .json file";
        dialog.showsResizeIndicator    = true;
        dialog.showsHiddenFiles        = false;
        dialog.canChooseDirectories    = true;
        dialog.canCreateDirectories    = true;
        dialog.allowsMultipleSelection = false;
        dialog.allowedFileTypes        = ["json"];
        
        if (dialog.runModal() == NSApplication.ModalResponse.OK) {
            let result = dialog.url // Pathname of the file
            
            if (result != nil) {
                let path = result!.path
                
                Logging.JLog(message: "path : \(path)")
                
                let people = JSONImporter.shared.importJSON(filePath: path)
                
                Logging.JLog(message: "people : \(people.count)")
                
                let dataGot = ["people" : people] as [String:Any]
            NotificationCenter.default.post(name:Notification.Name(rawValue:"com.talkingcucumbertltd.gotfile"), object: nil, userInfo: dataGot)
                
                
            }
        } else {
            // User clicked on "Cancel"
            return
        }
        
    }
    
}

