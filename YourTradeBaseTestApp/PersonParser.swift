//
//  PersonParser.swift
//  YourTradeBaseTestApp
//
//  Created by Joel Barker on 19/06/2018.
//  Copyright © 2018 Talking Cucumber Ltd. All rights reserved.
//

import Cocoa

class PersonParser: NSObject {

    var jsonDict = [String : Any] ()
    
    override init () {
    }
    
    init (theJsonDict: [String : Any]) {
        super.init()
        
        self.jsonDict = theJsonDict
    }
    
    func getId () -> String {
        
        var valueStr = ""
        
        if jsonDict ["id"] != nil {
            valueStr = jsonDict ["id"] as! String
        }
        
        return valueStr
    }

    func getValue () -> Decimal {
        var value = Decimal (0)
        
        if jsonDict ["value"] != nil {
            let valueStr = jsonDict ["value"] as! String
            value = Decimal (string: valueStr)!
        }
        
        return value
    }


    
    func getFirstName () -> String {
        var valueStr = ""
        
        if jsonDict ["name"] != nil {
            let nameDict = jsonDict ["name"] as! [String:String]
            valueStr = nameDict ["first"]! as String
        }
        
        return valueStr
    }

    func getLastName () -> String {
        var valueStr = ""
        
        if jsonDict ["name"] != nil {
            let nameDict = jsonDict ["name"] as! [String:String]
            valueStr = nameDict ["last"]! as String
        }
        
        return valueStr
    }

    func getLocation () -> [Double] {
        
        var latLon = [Double] ()
        
        if jsonDict ["location"] != nil {
            let nameDict = jsonDict ["location"] as! [String:String]
            let lat = nameDict ["latitude"]! as String
            let lon = nameDict ["longitude"]! as String
            
            let latDouble = Double (lat)
            let lonDouble = Double (lon)
            
            latLon.append(latDouble!)
            latLon.append(lonDouble!)
        }
        
        return latLon
        
    }

    func getCompany () -> String {
        var valueStr = ""
        
        if jsonDict ["value"] != nil {
            valueStr = jsonDict ["company"] as! String
        }
        
        return valueStr
    }
    
    func getEmail () -> String {
        var valueStr = ""
        
        if jsonDict ["value"] != nil {
            valueStr = jsonDict ["email"] as! String
        }
        
        return valueStr
    }
    
    func getAddress () -> String {
        var valueStr = ""
        
        if jsonDict ["value"] != nil {
            valueStr = jsonDict ["address"] as! String
        }
        
        return valueStr
    }
    
    func getCountry () -> String {
        var valueStr = ""
        
        if jsonDict ["value"] != nil {
            valueStr = jsonDict ["country"] as! String
        }
        
        return valueStr
    }
    
}

