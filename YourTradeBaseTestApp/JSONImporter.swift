//
//  JSONImporter.swift
//  YourTradeBaseTestApp
//
//  Created by Joel Barker on 19/06/2018.
//  Copyright © 2018 Talking Cucumber Ltd. All rights reserved.
//

import Cocoa

class JSONImporter: NSObject {

    static let shared = JSONImporter ()
    
    func importJSON (filePath: String) -> [Person] {
        
        var people = [Person] ()
        
        let file: FileHandle? = FileHandle(forReadingAtPath: filePath)
        
        let data = file?.readDataToEndOfFile()
        
        do {
            let jsonObj = try JSONSerialization.jsonObject(with: data!, options: [])
            
            let jsonArray = jsonObj as! [[String:Any]]
            
            //let jsonStr = String(decoding: data!, as: UTF8.self)
            
            for jsonData in jsonArray {
                
                let person = Person (theJsonDict: jsonData)
                Logging.JLog(message: "made person : \(person.description)")
                people.append(person)
            }
            
            Logging.JLog(message: "jsonArray \(jsonArray)")
            //Logging.JLog(message: "jsonStr \(jsonStr)")
            
        } catch  {
            print("error trying to convert JSON")
            return people
        }
        
        file?.closeFile()
        
        return people
    }
    
}
