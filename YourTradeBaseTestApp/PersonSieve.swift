//
//  PersonSifter.swift
//  YourTradeBaseTestApp
//
//  Created by Joel Barker on 19/06/2018.
//  Copyright © 2018 Talking Cucumber Ltd. All rights reserved.
//

import Cocoa

class PersonSieve: NSObject {

    override init() {
        
    }
    
    var people = [Person] ()
    
    var avgValue = Decimal (0)
    
    init (thePeople: [Person]) {
        self.people = thePeople
    }
    
    func findForLocation (latLon: [Double], distanceWanted: Double, countryWanted: String) -> [[String:Any]] {
        
        var peopleUnsorted = [Person] ()
        
        var peopleFound = [[String:Any]] ()
        
        self.avgValue = 0
        
        for person in self.people {
            
            let distance = person.distanceFrom(lat: latLon[0], lon: latLon[1])
            
            Logging.JLog(message: "person : \(person)")
            Logging.JLog(message: "distance : \(distance)")
            
            if distance < distanceWanted {
                
                if countryWanted != "" {
                    
                    Logging.JLog(message: "countryWanted : \(countryWanted.lowercased())")
                    Logging.JLog(message: "person.country : \(person.country.lowercased())")

                    if person.country.lowercased() == countryWanted.lowercased() {
                        peopleUnsorted.append(person)
                        self.avgValue = self.avgValue + person.value
                        Logging.JLog(message: "added")
                    }
                } else {
                    peopleUnsorted.append(person)
                    self.avgValue = self.avgValue + person.value
                }
            }
        }
        
        self.avgValue = self.avgValue / Decimal(peopleUnsorted.count)
        
        // sort by value
        peopleUnsorted.sort { $0.value > $1.value }
        
        print (peopleUnsorted)
        
        for person in peopleUnsorted {
            peopleFound.append(person.getBasicDetails())
        }
        
        return peopleFound
    }
    
    
    
    
}
